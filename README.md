# Shadow project


A simple yet powerful personal assistance web application.

## Primary apps/features

- [x] Blog
- [x] Contact Page
- [ ] Link Curator/Manager
- [x] Personal Expense Manager
- [x] Todo/Dones App
- [ ] Portfolio Manager or About me Page(s)
- [ ] Expense Sharing with Friends


##License
MIT License. See LICENSE file
