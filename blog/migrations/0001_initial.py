# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=250)),
                ('slug', models.SlugField(unique=True)),
                ('description', models.TextField()),
                ('description_html', models.TextField(blank=True, editable=False)),
            ],
            options={
                'verbose_name_plural': 'Categories',
                'ordering': ('title',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('pub_date', models.DateTimeField(auto_now_add=True, verbose_name='Date posted')),
                ('update', models.DateTimeField(auto_now=True, verbose_name='Date updated')),
                ('slug', models.SlugField(unique_for_date='pub_date')),
                ('status', models.IntegerField(default=1, choices=[(1, 'Live'), (2, 'Draft'), (3, 'Hidden')])),
                ('title', models.CharField(max_length=250)),
                ('body', models.TextField()),
                ('body_html', models.TextField(blank=True, editable=False)),
                ('excerpt', models.TextField(blank=True, null=True)),
                ('excerpt_html', models.TextField(blank=True, editable=False, null=True)),
                ('categories', models.ManyToManyField(to='blog.Category')),
            ],
            options={
                'get_latest_by': 'pub_date',
                'verbose_name_plural': 'Entries',
                'ordering': ('-pub_date',),
            },
            bases=(models.Model,),
        ),
    ]
