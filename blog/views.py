from django.views import generic

from blog.models import Category
from blog.models import Entry


class BaseEntryView(object):
    date_field = 'pub_date'
    model = Entry


class BaseCategoryView(object):
    model = Category


class NavMixin(object):

    def get_context_data(self, **kwargs):
        context = super(NavMixin, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all().order_by('title')
        return context


class BlogHome(NavMixin, BaseEntryView, generic.ListView):
    template_name = "blog/blog_home.html"
    paginate_by = 10
    context_object_name = 'entries'

    def get_queryset(self):
        return Entry.live.all()


class EntryArchiveIndex(NavMixin, BaseEntryView, generic.ArchiveIndexView):
    pass


class EntryDetail(NavMixin, BaseEntryView, generic.DetailView):
    pass


class CategoryList(NavMixin, BaseCategoryView, generic.ListView):
    pass


class CategoryDetail(NavMixin, BaseCategoryView, generic.DetailView):
    def get_context_data(self, **kwargs):
        context = super(CategoryDetail, self).get_context_data(**kwargs)
        context['entries'] = self.object.live_entries
        return context
