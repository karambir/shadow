"""
URLs for entries, categories and feeds in the blog.

"""

from django.conf.urls import patterns
from django.conf.urls import url

from blog import views

from blog.feeds import CategoryFeed
from blog.feeds import EntriesFeed

urlpatterns = patterns('',
                       url(r'^$',
                           views.BlogHome.as_view(),
                           name='blog_home'),
                       url(r'^archive/$',
                           views.EntryArchiveIndex.as_view(),
                           name='blog_entry_archive_index'),
                       url(r'^entry/(?P<slug>[-\w]+)/$',
                           views.EntryDetail.as_view(),
                           name='blog_entry_detail'),
                       )

urlpatterns += patterns('',
                        url(r'^category/$',
                            views.CategoryList.as_view(),
                            name='blog_category_list'),
                        url(r'^category/(?P<slug>[-\w]+)/$',
                            views.CategoryDetail.as_view(),
                            name='blog_category_detail'),
                        )

urlpatterns += patterns('',
                        url(r'^rss/$',
                            EntriesFeed(),
                            name='blog_feeds_entries'),
                        url(r'^rss/(?P<slug>[-\w]+)/$',
                            CategoryFeed(),
                            name='blog_feeds_category'),
                        )
