from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from shadow import views

urlpatterns = patterns('',
                       url(r'^$',
                           views.Home.as_view(),
                           name='home'),
                       url(r'^blog/',
                           include('blog.urls')),
                       url(r'^expenses/',
                           include('expenses.urls')),
                       url(r'^pages/',
                           include('django.contrib.flatpages.urls')),
                       url(r'^admin/',
                           include(admin.site.urls)),
                       url(r'^contact/',
                           include('contact.urls',
                               namespace='contact',
                               app_name='contact')),
                       )

urlpatterns += patterns('',
                        url('^20',
                            views.old_blog,
                            name = 'old_blog'),
                        )
