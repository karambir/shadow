from .settings import *

DEBUG = True
TEMPLATE_DEBUG = True

INSTALLED_APPS += (
    'django_extensions',
)

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

CONTACT_EMAILS = ['admin@example.com']
