from .settings import *

# Enforce secret key from environment
SECRET_KEY = os.environ.get("NAINOMICS_KEY")

ALLOWED_HOSTS = [
    '.nainomics.in',
]

CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True

CONN_MAX_AGE = True

import dj_email_url
email_config = dj_email_url.config()

EMAIL_FILE_PATH = email_config['EMAIL_FILE_PATH']
EMAIL_HOST_USER = email_config['EMAIL_HOST_USER']
EMAIL_HOST_PASSWORD = email_config['EMAIL_HOST_PASSWORD']
EMAIL_HOST = email_config['EMAIL_HOST']
EMAIL_PORT = email_config['EMAIL_PORT']
EMAIL_BACKEND = email_config['EMAIL_BACKEND']
EMAIL_USE_TLS = email_config['EMAIL_USE_TLS']

#Settings for myks-contact
CONTACT_EMAILS = ['akarambir@gmail.com']
