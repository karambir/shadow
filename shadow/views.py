from django.views import generic
from django.http import HttpResponse, HttpResponsePermanentRedirect, HttpResponseGone, Http404


class Home(generic.TemplateView):
    template_name = "home.html"

redirect_urls = {
    "/2012/10/beginners-guide-to-open-source-software.html": "/blog/entry/beginners-guide-open-source/",
    "/2012/09/startup-what-it-is.html": "/blog/entry/startup-what-it/",
    "/2012/09/thoughts-about-god.html": "/blog/entry/thoughts-about-god/",
    "/2012/03/power-of-open-source.html": "/blog/entry/power-open-source/",
    "/2011/12/tools-you-can-use-while-reading-on-web.html": "/blog/entry/tools-you-can-use-while-reading-web/",
    "/2011/12/google-should-and-will-succeed-as.html": "/blog/entry/google-should-and-will-succeed/",
    "/2011/12/technologies-you-should-know-for-web.html": "/blog/entry/technologies-you-should-know-web/",
    "/2011/12/what-blogging-means-to-me.html": "/blog/entry/what-blogging-means-me/",
    "/2011/11/passion-for-code.html": "/blog/entry/passion-code/",
    "/2011/11/speak-truth-to-stop-procrastination.html": "/blog/entry/speak-truth-stop-procastination/",
    "/2011/11/welcome-to-indias-higher-education.html": "/blog/entry/welcome-indias-higher-education/",
    "/2011/10/innovation-is-it-really-tough.html": "/blog/entry/innovation-it-really-tough/",
}

dead_urls = (
    "/2012/10/prepare-3-envelopes.html",
    "/2012/04/reblog-releasing-our-first-mvp.html",
    "/2012/01/indians-more-innovative-than-britishers.html",
    "/2012/01/want-to-become-codesters.html",
    "/2011/12/indias-unique-innovations.html",
    "/2011/12/mobile-web-apps-versus-native-apps.html",
    "/2011/12/7-habits-of-efficient-programmers.html",
    "/2011/12/introduction-to-algorithms-and.html",
    "/2011/12/learn-python-reading-list.html",
    "/2011/12/why-are-there-so-many-engineers-in.html",
    "/2011/12/3-types-of-innovation-structured-apple.html",
    "/2011/11/loopy-c-puzzle.html",
    "/2011/11/google-company-in-mid-life-crises.html",
    "/2011/11/indian-ecommerce-startups-their.html",
    "/2011/11/all-i-need-is-just-2-hours-to-get.html",
    "/2011/11/is-google-doing-enough-for-entering-in.html",
    "/2011/11/wow-google-offers-free-website-with-in.html",
)

def old_blog(request):
    response = "This url is removed " + request.path
    if request.path in redirect_urls.keys():
        return HttpResponsePermanentRedirect(redirect_urls[request.path])
    elif request.path in dead_urls:
        return HttpResponseGone(response)
    else:
        raise Http404()
