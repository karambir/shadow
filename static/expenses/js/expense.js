(function($){
    jQuery = $.noConflict(true);

    function showReport(options) {

    }

    $(document).ready(function() {
	$(".reportlink").click(function (e) {
	    var x = $(this).attr('id').split("-");
        var charturl = $(this).attr('data-url');

	    var title = $(this).text()
	    title += x[2]=='None'?'': x[2] + '. ';
	    title += x[3]=='None'?'':x[3]+'.';

            var d = $('#dialog').html('<iframe id="ifrm" width="650" frameborder="0" height="330"></iframe>');
            d.dialog({
		modal: true,
		resizable: false,
		width: 680,
		title: title
	    });

	    $("#dialog>#ifrm").attr("src", charturl);
	});
    });

})(django.jQuery);
