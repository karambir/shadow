from django.contrib import admin

from dones.models import DoneType, Done


class DoneTypeAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}

class DoneAdmin(admin.ModelAdmin):
    date_hierarchy = 'date_created'

    fieldsets = (
        ('Details', {
            'fields': ('title', 'description', 'done_type')
        }),
        ('Status', {
            'fields': ('estimate', 'completed')
        })

    )

    list_display = ('title', 'date_created', 'done_type', 'completed')
    list_display_links = ('title',)
    list_filter = ('completed',)
    search_fields = ('title', 'description', 'estimate')


admin.site.register(DoneType, DoneTypeAdmin)
admin.site.register(Done, DoneAdmin)
