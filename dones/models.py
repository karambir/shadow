from django.db import models


class DoneType(models.Model):
    name = models.CharField(max_length=40)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.name


class Done(models.Model):
    """
    A todo or done in all things done app
    """

    date_created = models.DateTimeField('Date added', auto_now_add=True)
    date_updated = models.DateTimeField('Date modified', auto_now=True)
    title = models.CharField(max_length=250)
    description = models.TextField(blank=True)

    done_type = models.ForeignKey(DoneType)
    estimate = models.CharField(max_length=12,
                                help_text="h:hour,d:day,m:month,y:year",
                                default="0")
    completed = models.BooleanField(default=True)

    class Meta:
        ordering = ['-date_updated']

    def __str__(self):
        return self.title
