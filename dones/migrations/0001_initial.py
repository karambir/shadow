# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Done',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name=b'Date added')),
                ('date_updated', models.DateTimeField(auto_now=True, verbose_name=b'Date modified')),
                ('title', models.CharField(max_length=250)),
                ('description', models.TextField(blank=True)),
                ('estimate', models.CharField(default=b'0', help_text=b'h:hour,d:day,m:month,y:year', max_length=12)),
                ('completed', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DoneType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=40)),
                ('slug', models.SlugField(unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='done',
            name='done_type',
            field=models.ForeignKey(to='dones.DoneType'),
            preserve_default=True,
        ),
    ]
