# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dones', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='done',
            name='date_created',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Date added'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='done',
            name='date_updated',
            field=models.DateTimeField(auto_now=True, verbose_name='Date modified'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='done',
            name='estimate',
            field=models.CharField(default='0', help_text='h:hour,d:day,m:month,y:year', max_length=12),
            preserve_default=True,
        ),
    ]
