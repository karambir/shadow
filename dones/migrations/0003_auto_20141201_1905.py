# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dones', '0002_auto_20141128_1111'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='done',
            options={'ordering': ['-date_updated']},
        ),
    ]
