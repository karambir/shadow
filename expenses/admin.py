from datetime import datetime
from urllib.parse import urlparse

from django.contrib import admin
from django.utils.datastructures import MultiValueDictKeyError
from django.conf import settings

from expenses.models import Type, Category, Expense
from expenses.filters import ExpenseDateListFilter

def get_year(request):
    """
    Returns the querystring 'date__year' parameter or the current
    year. If it was not found then returns with `None`.
    """
    try:
        return int(request.GET['date__year'])
    except (MultiValueDictKeyError, ValueError):
        return None

def get_month(request):
    """
    Returns the querystring 'date__month' parameter or the current month.
    If it was not found then returns with `None`.
    """
    try:
        return int(request.GET['date__month'])
    except (MultiValueDictKeyError, ValueError):
        return None

class TypeAdmin(admin.ModelAdmin):
    """
    Admin interface for Expense groups.
    """
    list_display = ('name', )
    search_fields = ('name', )
    ordering = ('name', )

admin.site.register(Type, TypeAdmin)

class CategoryAdmin(admin.ModelAdmin):
    """
    Admin interface for expense categories.
    """
    list_display = ('type_name', 'name', )
    list_display_links = ('name', )
    search_fields = ('type', 'name', )
    ordering = ('type__name','name', )

admin.site.register(Category, CategoryAdmin)

class ExpenseAdmin(admin.ModelAdmin):
    """
    Admin interface for expenses.
    """
    class Media:
        css = {
            "all": (
                settings.STATIC_URL + "expenses/css/expense.css",
                settings.STATIC_URL + "expenses/css/cupertino/jquery-ui.min.css",
                )
        }
        js = (settings.STATIC_URL + "expenses/js/expense.js",
              settings.STATIC_URL + "expenses/js/jquery-ui.min.js"
              )

    list_display = ('date_str',
                    'category',
                    'description',
                    'formatted_amount')

    list_filter = (('date', ExpenseDateListFilter), 'category', )
    ordering = ('-date', )

    def changelist_view(self, request, extra_context=None):
        """
        Extends the changelist_view with the following parameters:
        - statistics
        - selected year, month
        - title
        """
        year = get_year(request)
        month = get_month(request)
        # setting default parmeters if we did not came from the changelist
        # view.
        referer = urlparse(request.META.get('HTTP_REFERER', ''))
        if(referer.path != request.META.get('PATH_INFO', '')):
            year = datetime.now().year
            month = datetime.now().month
            q = request.GET.copy()
            q['date__year'] = str(year)
            q['date__month'] = str(month)
            request.GET = q
            request.META['QUERY_STRING'] = request.GET.urlencode()

        types_stat = Type.objects.get_statistics(year, month)
        title = 'Overall expenses'
        if year:
            title = ('Expenses of %s' % year)
        if month:
             d = datetime(year, month, 1)
             month_name = d.strftime("%B")
             title = ('Expenses of %s %s' % (month_name, year))

        extra_context = {
            'types_stat' : types_stat,
            'current_year': year,
            'current_month': month,
            'title': title,
        }

        return admin.ModelAdmin.changelist_view(self, request, extra_context)

admin.site.register(Expense, ExpenseAdmin)
