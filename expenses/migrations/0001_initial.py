# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=50, verbose_name='name')),
            ],
            options={
                'ordering': ['type__name', 'name'],
                'verbose_name_plural': 'categories',
                'verbose_name': 'category',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Expense',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('date', models.DateField(verbose_name='date')),
                ('description', models.CharField(max_length=300, verbose_name='description')),
                ('amount', models.IntegerField(verbose_name='amount')),
                ('category', models.ForeignKey(blank=True, to='expenses.Category', related_name='expenses', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ['-date', 'category__type__name', 'category__name'],
                'verbose_name_plural': 'expenses',
                'verbose_name': 'expense',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Type',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(max_length=50, verbose_name='name')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name_plural': 'types',
                'verbose_name': 'type',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='category',
            name='type',
            field=models.ForeignKey(to='expenses.Type', related_name='categories', null=True),
            preserve_default=True,
        ),
    ]
