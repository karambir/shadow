from django.conf.urls import patterns, url
from expenses import views

urlpatterns = patterns('',
    url(r'^pie_chart_data/(?P<type>\d+)/(?P<year>\d+)/(?P<month>\d+)',
        views.pie_chart_data),
)
