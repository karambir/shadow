from django.db.models import Sum, Q
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.conf import settings
import json

from expenses.models import Type

def pie_chart_data(request, type, year, month):
    expense_type =  Type.objects.get(id = type)
    data =  _get_pie_chart_data(expense_type, year, month)
    c = {
        'type': expense_type.name,
        'year':  year,
        'month': month,
        'chart_data': json.dumps(data),
    }
    print(c)
    return render_to_response('admin/expenses/expense/pie_chart.html',
                                c,
                                context_instance=RequestContext(request)
    )

def _get_pie_chart_data(chart_type, year = None, month = None):
    results = []
    q = Q()
    if year and int(year) > 0:
        q = q & Q(expenses__date__year = year)
    if month and int(month) > 0:
        q = q & Q(expenses__date__month = month)

    #calculate values

    chart_type.category_list = chart_type.categories.filter(q).annotate(
        category_total = Sum('expenses__amount'))

    #format the results
    for c in chart_type.category_list:
        results.append({
                'label': c.name,
                'data': c.category_total
                })

    return results
